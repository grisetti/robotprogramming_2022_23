# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src

# Include any dependencies generated for this target.
include CMakeFiles/mat_f_test.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/mat_f_test.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/mat_f_test.dir/flags.make

CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o: CMakeFiles/mat_f_test.dir/flags.make
CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o: mat_f_test.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o -c /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/mat_f_test.cpp

CMakeFiles/mat_f_test.dir/mat_f_test.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mat_f_test.dir/mat_f_test.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/mat_f_test.cpp > CMakeFiles/mat_f_test.dir/mat_f_test.cpp.i

CMakeFiles/mat_f_test.dir/mat_f_test.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mat_f_test.dir/mat_f_test.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/mat_f_test.cpp -o CMakeFiles/mat_f_test.dir/mat_f_test.cpp.s

CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.requires:

.PHONY : CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.requires

CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.provides: CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.requires
	$(MAKE) -f CMakeFiles/mat_f_test.dir/build.make CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.provides.build
.PHONY : CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.provides

CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.provides.build: CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o


CMakeFiles/mat_f_test.dir/mat_f.cpp.o: CMakeFiles/mat_f_test.dir/flags.make
CMakeFiles/mat_f_test.dir/mat_f.cpp.o: mat_f.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/mat_f_test.dir/mat_f.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/mat_f_test.dir/mat_f.cpp.o -c /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/mat_f.cpp

CMakeFiles/mat_f_test.dir/mat_f.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mat_f_test.dir/mat_f.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/mat_f.cpp > CMakeFiles/mat_f_test.dir/mat_f.cpp.i

CMakeFiles/mat_f_test.dir/mat_f.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mat_f_test.dir/mat_f.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/mat_f.cpp -o CMakeFiles/mat_f_test.dir/mat_f.cpp.s

CMakeFiles/mat_f_test.dir/mat_f.cpp.o.requires:

.PHONY : CMakeFiles/mat_f_test.dir/mat_f.cpp.o.requires

CMakeFiles/mat_f_test.dir/mat_f.cpp.o.provides: CMakeFiles/mat_f_test.dir/mat_f.cpp.o.requires
	$(MAKE) -f CMakeFiles/mat_f_test.dir/build.make CMakeFiles/mat_f_test.dir/mat_f.cpp.o.provides.build
.PHONY : CMakeFiles/mat_f_test.dir/mat_f.cpp.o.provides

CMakeFiles/mat_f_test.dir/mat_f.cpp.o.provides.build: CMakeFiles/mat_f_test.dir/mat_f.cpp.o


CMakeFiles/mat_f_test.dir/vec_f.cpp.o: CMakeFiles/mat_f_test.dir/flags.make
CMakeFiles/mat_f_test.dir/vec_f.cpp.o: vec_f.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Building CXX object CMakeFiles/mat_f_test.dir/vec_f.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/mat_f_test.dir/vec_f.cpp.o -c /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/vec_f.cpp

CMakeFiles/mat_f_test.dir/vec_f.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/mat_f_test.dir/vec_f.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/vec_f.cpp > CMakeFiles/mat_f_test.dir/vec_f.cpp.i

CMakeFiles/mat_f_test.dir/vec_f.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/mat_f_test.dir/vec_f.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/vec_f.cpp -o CMakeFiles/mat_f_test.dir/vec_f.cpp.s

CMakeFiles/mat_f_test.dir/vec_f.cpp.o.requires:

.PHONY : CMakeFiles/mat_f_test.dir/vec_f.cpp.o.requires

CMakeFiles/mat_f_test.dir/vec_f.cpp.o.provides: CMakeFiles/mat_f_test.dir/vec_f.cpp.o.requires
	$(MAKE) -f CMakeFiles/mat_f_test.dir/build.make CMakeFiles/mat_f_test.dir/vec_f.cpp.o.provides.build
.PHONY : CMakeFiles/mat_f_test.dir/vec_f.cpp.o.provides

CMakeFiles/mat_f_test.dir/vec_f.cpp.o.provides.build: CMakeFiles/mat_f_test.dir/vec_f.cpp.o


# Object files for target mat_f_test
mat_f_test_OBJECTS = \
"CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o" \
"CMakeFiles/mat_f_test.dir/mat_f.cpp.o" \
"CMakeFiles/mat_f_test.dir/vec_f.cpp.o"

# External object files for target mat_f_test
mat_f_test_EXTERNAL_OBJECTS =

mat_f_test: CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o
mat_f_test: CMakeFiles/mat_f_test.dir/mat_f.cpp.o
mat_f_test: CMakeFiles/mat_f_test.dir/vec_f.cpp.o
mat_f_test: CMakeFiles/mat_f_test.dir/build.make
mat_f_test: CMakeFiles/mat_f_test.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/CMakeFiles --progress-num=$(CMAKE_PROGRESS_4) "Linking CXX executable mat_f_test"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/mat_f_test.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/mat_f_test.dir/build: mat_f_test

.PHONY : CMakeFiles/mat_f_test.dir/build

CMakeFiles/mat_f_test.dir/requires: CMakeFiles/mat_f_test.dir/mat_f_test.cpp.o.requires
CMakeFiles/mat_f_test.dir/requires: CMakeFiles/mat_f_test.dir/mat_f.cpp.o.requires
CMakeFiles/mat_f_test.dir/requires: CMakeFiles/mat_f_test.dir/vec_f.cpp.o.requires

.PHONY : CMakeFiles/mat_f_test.dir/requires

CMakeFiles/mat_f_test.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/mat_f_test.dir/cmake_clean.cmake
.PHONY : CMakeFiles/mat_f_test.dir/clean

CMakeFiles/mat_f_test.dir/depend:
	cd /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src /media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src/CMakeFiles/mat_f_test.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/mat_f_test.dir/depend

