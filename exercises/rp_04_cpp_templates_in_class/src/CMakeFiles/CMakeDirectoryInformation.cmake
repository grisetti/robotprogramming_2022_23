# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Relative path conversion top directories.
set(CMAKE_RELATIVE_PATH_TOP_SOURCE "/media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class")
set(CMAKE_RELATIVE_PATH_TOP_BINARY "/media/giorgio/data/teaching/teaching/robotprogramming_2022_23/exercises/rp_04_cpp_templates_in_class/src")

# Force unix paths in dependencies.
set(CMAKE_FORCE_UNIX_PATHS 1)


# The C and CXX include file regular expressions for this directory.
set(CMAKE_C_INCLUDE_REGEX_SCAN "^.*$")
set(CMAKE_C_INCLUDE_REGEX_COMPLAIN "^$")
set(CMAKE_CXX_INCLUDE_REGEX_SCAN ${CMAKE_C_INCLUDE_REGEX_SCAN})
set(CMAKE_CXX_INCLUDE_REGEX_COMPLAIN ${CMAKE_C_INCLUDE_REGEX_COMPLAIN})
